import React, { Component } from 'react'

export const DataContext = React.createContext();

export class DataProvider extends Component {

    state = {
        products: [
            {
                "_id": "1",
                "title": "Tênis Nike Revolution 6",
                "src": "https://static.netshoes.com.br/produtos/tenis-nike-revolution-6-next-nature-masculino/26/2IC-4365-026/2IC-4365-026_zoom2.jpg?ts=1631636825&ims=326x",
                "description": "Indicado para: Dia a Dia",
                "content": "Para caminhadas e corridas leves, treinos de musculação ou até mesmo no dia a dia aposte no conforto e qualidade do Tênis Nike Masculino Revolution 6 Next Nature para completar seu visual. Com cabedal produzido em material macio, o modelo possui calcanhar acolchoado e fecho em cadarço para ajuste personalizado e firme. A entressola em EVA proporciona amortecimento leve e o solado emborrachado garante aderência e tração em diferentes tipos de pisos. Já o visual moderno agrega valor e deixa seu estilo autêntico e despojado. Aproveite!",
                "price": 224.99,
                "colors": ["red", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "2",
                "title": "Tênis Nike Downshifter 11",
                "src": "https://static.netshoes.com.br/produtos/tenis-nike-downshifter-11-feminino/12/HZM-5209-112/HZM-5209-112_zoom1.jpg?ts=1650974167&ims=544x",
                "description": "Indicado para: Caminhada, Corrida",
                "content": "Para caminhadas e corridas leves na esteira ou nas ruas, aposte no conforto e qualidade do Tênis Nike Feminino para completar seu visual esportivo e treinar em busca do corpo saudável. Com um cabedal produzido em malha macia e respirável, o modelo possui tiras sintéticas que, junto ao calcanhar, garantem conforto e suporte para você ir mais longe. A entressola alta e fabricada em EVA proporciona amortecimento leve e deixa a passada mais suave. Já o solado emborrachado é aderente a diferentes tipos de piso, oferecendo segurança para você ultrapassar seus limites e ir cada vez mais longe. Se você é iniciante na corrida e está procurando por um tênis que faça a diferença, o novo Nike Downshifter 11 é a escolha certa. Aproveite!",
                "price": 215.99,
                "colors": ["red", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "3",
                "title": "Tênis Nike Revolution 6 Next",
                "src": "https://static.netshoes.com.br/produtos/tenis-nike-revolution-6-next-nature-masculino/74/2IC-4365-174/2IC-4365-174_zoom1.jpg?ts=1643305626&ims=544x",
                "description": "Indicado Para: Dia a Dia",
                "content": "ESTILO VELOZ. JOGO VELOZ.Conheça a próxima geração de desempenho em tênis. Criado com base em anos de testes com atletas, o NikeCourt React Vapor NXT proporciona tração de primeira linha e um sistema de amortecimento inovador para que você possa dar o seu melhor durante as partidas mais rápidas e ferozes.Design baseado em dadosDesenvolvido para suportar a força do deslizamento, o design generativo usa dados de testes para integrar borracha durável e plástico resistente em áreas de alto desgaste, como o lado medial do antepé.Mais próximo do chãoA tecnologia Nike React proporciona uma passada incrivelmente estável. Posicionada perto do planta do pé, a espuma macia trabalha em conjunto com uma espuma mais firme ao longo da parte externa do pé para ajudar a aproximá-lo do chão. Esse alinhamento foi projetado para ajudar você a se impulsionar durante movimentos rápidos.Tração inovadoraA sola em padrão espinha de peixe utiliza um design baseado em dados para proporcionar uma aderência ideal sem comprometer sua capacidade de deslizar.Mais benefíciosA estrutura rígida na lateral cria estabilidade adicional durante movimentos laterais.A manga de comprimento total proporciona um caimento confortável e semelhante a uma meia.Detalhes do produtoProjetado para uso em superfícies de quadra duraPalmilha removívelPuxador no calcanharTamanho & Caimento Formato largo; recomendamos pedir meio tamanho a menos",
                "price": 224.99,
                "colors": ["lightblue", "white", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "4",
                "title": "Chuteira Nike Mercurial Air",
                "src": "https://static.netshoes.com.br/produtos/chuteira-nike-mercurial-air-zoom-vapor-14-pro-masculino/10/2IC-8233-010/2IC-8233-010_zoom1.jpg?ts=1651765091&ims=544x",
                "description": "Campo Society",
                "content": "Liberte a sua velocidade com a Nike Mercurial Air Zoom Vapor 14 Pro. O design refinado usa a menor quantidade possível de materiais para ajudar a manter a velocidade do início ao fim.Conforto com amortecimentoA unidade Zoom Air no calcanhar proporciona mais amortecimento reativo e agilidade a cada passo.Tração versátilBaseada em análises de pesquisas sobre os movimentos dos jogadores, a sola de borracha oferece tração multidirecional.Design durávelA sobreposição HyperScreen proporciona a durabilidade necessária para encarar a temporada.Detalhes do produtoPara uso em superfícies sintéticas curtasPalmilha com amortecimento",
                "price": 1099.99,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "5",
                "title": "Tênis NikeCourt React",
                "src": "https://static.netshoes.com.br/produtos/tenis-nikecourt-react-vapor-nxt-feminino/14/2IC-7285-014/2IC-7285-014_zoom1.jpg?ts=1645704917&ims=544x",
                "description": "Indicado para: Dia a Dia",
                "content": "ESTILO VELOZ. JOGO VELOZ.Para o rally que não cede ou para o vôlei agitado na rede, escolha a próxima geração de tênis para o treino. Baseado em anos de dados de atletas, ele fornece o tipo de tração tenaz para ajudar você a alcançar a bola em qualquer lance pela linha de base. Um sistema de amortecimento inovador que oferece sustentação satisfatória para fazer a diferença durante a partida mais difícil.Mais próximo do chãoA tecnologia Nike React garante uma passada incrivelmente estável. Posicionada perto do planta do pé, a espuma macia trabalha com uma espuma mais firme ao longo da parte externa do pé. Esse alinhamento foi projetado para ajudar você a se impulsionar durante movimentos rápidos.Durabilidade baseada em dadosConstruído para suportar a força do deslizamento, o design generativo usa dados para integrar sua borracha durável e seu plástico resistente em áreas de alto desgaste, como o lado medial do antepé.Tração inovadoraA sola em padrão de espinha de peixe modificada utiliza dados de atletas para proporcionar aderência sem afetar a sua capacidade de deslizar.Mais benefíciosA estrutura rígida na lateral oferece uma sensação de estabilidade durante movimentos laterais.A manga interna de comprimento total proporciona um caimento confortável e semelhante a uma meia.Detalhes do produtoProjetado para uso em superfícies de quadra duraPalmilha removívelPuxador no calcanharTamanho & Caimento Formato largo; recomendamos pedir meio tamanho a menos",
                "price": 1049.99,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            },
            {
                "_id": "6",
                "title": "Chuteira Campo Nike Phantom",
                "src": "https://static.netshoes.com.br/produtos/chuteira-campo-nike-phantom-gt2-pro/42/2IC-3519-042/2IC-3519-042_zoom1.jpg?ts=1643990296&ims=544x",
                "description": "Indicado para: Chuteira Campo",
                "content": "Brilhe dentro das quatro linhas jogando com a nova Chuteira de Campo Nike. A Phantom GT2 Pro possui cabedal texturizado que proporciona melhor controle de bola, além de contar com o cadarço posicionado de forma descentralizada, criando uma zona livre para você dar chutes ainda mais poderosos. O colarinho é elástico para fácil calce e promove um ajuste dinâmico que elimina o espaço entre o pé e o calçado para ainda mais conforto. O modelo traz também uma tira em EVA localizada na entressola para amortecimento leve, além de solado emborrachado com travas altas para aderência e tração em campos de grama natural. Por fim, o visual da Nike Campo Phantom agrega valor e deixa o estilo dos boleiros e boleiras diferenciado. Garanta já a sua e marque esse golaço.",
                "price": 999.99,
                "colors": ["orange", "black", "crimson", "teal"],
                "count": 1
            }
        ],
        cart: [],
        total: 0
        
    };

    addCart = (id) =>{
        const {products, cart} = this.state;
        const check = cart.every(item =>{
            return item._id !== id
        })
        if(check){
            const data = products.filter(product =>{
                return product._id === id
            })
            this.setState({cart: [...cart,...data]})
        }else{
            alert("Produto ja adicionado ao carrinho.")
        }
    };

    reduction = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count === 1 ? item.count = 1 : item.count -=1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    increase = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count += 1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    removeProduct = id =>{
        if(window.confirm("Deseja excluir este produto?")){
            const {cart} = this.state;
            cart.forEach((item, index) =>{
                if(item._id === id){
                    cart.splice(index, 1)
                }
            })
            this.setState({cart: cart});
            this.getTotal();
        }
       
    };

    getTotal = ()=>{
        const{cart} = this.state;
        const res = cart.reduce((prev, item) => {
            return prev + (item.price * item.count);
        },0)
        this.setState({total: res})
    };
    
    componentDidUpdate(){
        localStorage.setItem('dataCart', JSON.stringify(this.state.cart))
        localStorage.setItem('dataTotal', JSON.stringify(this.state.total))
    };

    componentDidMount(){
        const dataCart = JSON.parse(localStorage.getItem('dataCart'));
        if(dataCart !== null){
            this.setState({cart: dataCart});
        }
        const dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
        if(dataTotal !== null){
            this.setState({total: dataTotal});
        }
    }
   

    render() {
        const {products, cart,total} = this.state;
        const {addCart,reduction,increase,removeProduct,getTotal} = this;
        return (
            <DataContext.Provider 
            value={{products, addCart, cart, reduction,increase,removeProduct,total,getTotal}}>
                {this.props.children}
            </DataContext.Provider>
        )
    }
}


